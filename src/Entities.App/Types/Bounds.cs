using System;

namespace Entities.App.Types
{
    public class Bounds
    {
        public static Bounds Zero => new Bounds(0, 0); 
        public int Width { get; }
        public int Height { get; }

        public Bounds(int width, int height)
        {
            Width = width;
            Height = height;
        }

        public bool Contains(Index index)
        {
            return 0 <= index.X && 0 <= index.Y && index.X < Width && index.Y < Height;
        }

        public static Bounds operator *(Bounds size, float scale)
        {
            return new Bounds((int)(size.Width * scale), (int)(size.Height * scale));
        }

        public static Size operator *(Bounds size, Size scale)
        {
            return new Size(size.Width * scale.Width, size.Height * scale.Height);
        }

        public static Bounds operator *(Bounds size, Bounds scale)
        {
            return new Bounds(size.Width * scale.Width, size.Height * scale.Height);
        }

        public static Bounds operator /(Bounds size, float scale)
        {
            return new Bounds((int)(size.Width / scale), (int)(size.Height / scale));
        }

        public static bool operator ==(Bounds a, Bounds b)
        {
            var aIsNull = a is null;
            var bIsNull = b is null;

            if (aIsNull && bIsNull)
                return true;

            if (aIsNull || bIsNull)
                return false;

            return Math.Abs(a.Width - b.Width) < float.Epsilon && Math.Abs(a.Height - b.Height) < float.Epsilon;
        }

        public static bool operator !=(Bounds a, Bounds b)
        {
            return !(a == b);
        }

        public bool Equals(Bounds other)
        {
            return Width.Equals(other.Width) && Height.Equals(other.Height);
        }

        public override bool Equals(object obj)
        {
            if (obj is null)
                return false;

            return obj is Bounds size && Equals(size);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Width.GetHashCode() * 397) ^ Height.GetHashCode();
            }
        }

        public Size ToSize()
        {
            return new Size(Width, Height);
        }

        public override string ToString()
        {
            return $"{GetType().Name}({Width},{Height})";
        }
    }
}
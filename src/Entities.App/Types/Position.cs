using System;
using Microsoft.Xna.Framework;

namespace Entities.App.Types
{
    public sealed class Position
    {
        public static Position Zero => new Position(0, 0);

        public float X { get; }
        public float Y { get; }

        public Position(Size size) : this(size.Width, size.Height) { }

        public Position(Index index) : this(index.X, index.Y) { }

        public Position(float x, float y)
        {
            X = x;
            Y = y;
        }

        public static Position operator *(Position a, float b)
        {
            return new Position(a.X * b, a.Y * b);
        }

        public static Position operator *(float b, Position a)
        {
            return new Position(a.X * b, a.Y * b);
        }

        public static Position operator /(Position a, float scale)
        {
            return new Position(a.X / scale, a.Y / scale);
        }

        public static Position operator /(Position a, Size size)
        {
            return new Position(a.X / size.Width, a.Y / size.Height);
        }

        public static Position operator +(Position a, Size b)
        {
            return new Position(a.X + b.Width, a.Y + b.Height);
        }

        public static Position operator +(Position a, Vector b)
        {
            return new Position(a.X + b.X, a.Y + b.Y);
        }

        public static Position operator -(Position a, Size b)
        {
            return new Position(a.X - b.Width, a.Y - b.Height);
        }

        public static Position operator +(Position a, Position b)
        {
            return new Position(a.X + b.X, a.Y + b.Y);
        }

        public static Position operator +(Position a, Index b)
        {
            return new Position(a.X + b.X, a.Y + b.Y);
        }

        public static Position operator +(Position a, Vector2 b)
        {
            return new Position(a.X + b.X, a.Y + b.Y);
        }
        
        public static Position operator -(Position a, Vector b)
        {
            return new Position(a.X - b.X, a.Y - b.Y);
        }

        public static Position operator -(Position a, Position b)
        {
            return new Position(a.X - b.X, a.Y - b.Y);
        }

        public static bool operator ==(Position a, Position b)
        {
            var aIsNull = a is null;
            var bIsNull = b is null;

            if (aIsNull && bIsNull)
                return true;

            if (aIsNull || bIsNull)
                return false;

            return Math.Abs(a.X - b.X) < float.Epsilon && Math.Abs(a.Y - b.Y) < float.Epsilon;
        }

        public static bool operator !=(Position a, Position b)
        {
            return !(a == b);
        }

        public static float DotProduct(Position a, Position b)
        {
            return a.X * b.X + a.Y * b.Y;
        }

        public static float DotProduct(Position a, Vector b)
        {
            return a.X * b.X + a.Y * b.Y;
        }

        public float Length()
        {
            return (float)Math.Sqrt(X * X + Y * Y);
        }

        public static Position Round(Position p)
        {
            return new Position((float)Math.Round(p.X), (float)Math.Round(p.Y));
        }

        public static float Distance(Position a, Position b)
        {
            return (float)Math.Sqrt(Math.Pow(b.X - a.X, 2) + Math.Pow(b.Y - a.Y, 2));
        }

        public static float DistanceSquared(Position a, Position b)
        {
            return (float)(Math.Pow(b.X - a.X, 2) + Math.Pow(b.Y - a.Y, 2));
        }

        public Position Normalise()
        {
            return this / Length();
        }

        private bool Equals(Position other)
        {
            return X.Equals(other.X) && Y.Equals(other.Y);
        }

        public override string ToString()
        {
            return $"{GetType().Name}({X},{Y})";
        }

        public override bool Equals(object obj)
        {
            if (obj is null)
                return false;

            if (ReferenceEquals(this, obj))
                return true;

            return obj is Position position && Equals(position);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (X.GetHashCode() * 397) ^ Y.GetHashCode();
            }
        }

        public Index ToIndex()
        {
            return new Index((int)X, (int)Y);
        }

        public Vector3 ToVector3()
        {
            return new Vector3(X, 0, Y);
        }

        public Vector2 ToVector2()
        {
            return new Vector2(X, Y);
        }
    }
}
﻿using Autofac;
using Entities.App.Assets;
using Entities.App.Events;
using Entities.App.Input;
using Entities.App.Input.Options;
using Entities.App.Rendering;
using Entities.App.Units;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Entities.App
{
    public class Game1 : Game
    {
        private readonly GraphicsDeviceManager _graphics;
        private Engine _engine;

        public Game1()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        protected override void Initialize()
        {
            var containerBuilder = new ContainerBuilder();

            containerBuilder
                .RegisterInstance(Content)
                .AsSelf()
                .SingleInstance();

            containerBuilder
                .RegisterInstance(GraphicsDevice)
                .AsSelf()
                .SingleInstance();

            containerBuilder
                .Register(x => new SpriteBatch(x.Resolve<GraphicsDevice>()))
                .AsSelf()
                .SingleInstance();

            containerBuilder
                .Register(x => AssetLoader.From(x.Resolve<GraphicsDevice>(), x.Resolve<ContentManager>()))
                .AsSelf()
                .SingleInstance();

            containerBuilder
                .RegisterType<Renderer>()
                .AsSelf()
                .SingleInstance();

            containerBuilder
                .RegisterType<Renderables>()
                .AsSelf()
                .AsImplementedInterfaces()
                .SingleInstance();

            containerBuilder
                .RegisterType<UnitController>()
                .AsSelf()
                .AsImplementedInterfaces()
                .SingleInstance();

            containerBuilder
                .RegisterType<Engine>()
                .AsSelf()
                .SingleInstance();

            containerBuilder
                .Register(x => x.Resolve<GraphicsDevice>().Viewport)
                .AsSelf()
                .SingleInstance();

            containerBuilder
                .RegisterInstance(new InputOptions { Type = InputType.OnKeyUp })
                .AsSelf()
                .SingleInstance();

            containerBuilder
                .RegisterType<InputDetector>()
                .AsSelf()
                .AsImplementedInterfaces()
                .SingleInstance();

            var container = containerBuilder.Build();

            DomainEvents.InitialiseFrom(container);

            _engine = container
                .Resolve<Engine>()
                .Initialise();

            base.Initialize();
        }

        protected override void LoadContent()
        {
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            _engine.Update(gameTime);
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            _engine.Draw(gameTime);
            base.Draw(gameTime);
        }
    }
}
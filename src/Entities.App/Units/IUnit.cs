﻿using Entities.App.Types;
using Microsoft.Xna.Framework;

namespace Entities.App.Units
{
    public interface IUnit
    {
        void MoveTo(Position position);
        void Update(GameTime gameTime);
    }
}
﻿using Entities.App.Types;

namespace Entities.App.Units.Movement
{
    public class NoMovement : IMovement
    {
        public IMovement Update(ref Position position)
        {
            return this;
        }
    }
}
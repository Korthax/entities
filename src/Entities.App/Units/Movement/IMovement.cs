﻿using Entities.App.Types;

namespace Entities.App.Units.Movement
{
    public interface IMovement
    {
        IMovement Update(ref Position position);
    }
}
﻿using Entities.App.Types;

namespace Entities.App.Units.Movement
{
    public class ToPositionMovement : IMovement
    {
        private readonly Position _target;
        private readonly float _speed;

        public ToPositionMovement(Position target, float speed)
        {
            _target = target;
            _speed = speed;
        }

        public IMovement Update(ref Position position)
        {
            var distance = new Vector(position - _target);

            if (distance.Length() < 1)
                return new NoMovement();

            var vector = Vector.Normalize(distance) * _speed;
            position -= vector;

            return this;
        }
    }
}
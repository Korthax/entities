﻿using System;
using System.Collections.Generic;
using System.Linq;
using Entities.App.Events;
using Entities.App.Input;
using Entities.App.Input.Events;
using Entities.App.Types;
using Entities.App.Units.Movement;
using Microsoft.Xna.Framework;

namespace Entities.App.Units
{
    public class UnitController : IEventHandler<InputOccurredEvent>
    {
        private readonly HashSet<IUnit> _units;

        public UnitController()
        {
            _units = new HashSet<IUnit>();
        }

        public void SpawnUnit(Position position)
        {
            var unit = new Unit(new NoMovement(), position);
            DomainEvents.Publish(new UnitSpawnedEvent(unit));
            _units.Add(unit);
        }

        public void Handle(InputOccurredEvent domainEvent)
        {
            if (domainEvent.ButtonSet.Contains(MouseButtons.RightButton))
                _units.First().MoveTo(domainEvent.MousePosition);
        }

        public void Update(GameTime gameTime)
        {
            foreach (var unit in _units)
                unit.Update(gameTime);
        }
    }
}
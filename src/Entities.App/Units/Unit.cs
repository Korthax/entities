﻿using System;
using Entities.App.Rendering;
using Entities.App.Types;
using Entities.App.Units.Movement;
using Microsoft.Xna.Framework;

namespace Entities.App.Units
{
    public class Unit : IUnit, IRenderable
    {
        private readonly Guid _id;

        private IMovement _movement;
        private Position _position;

        public Unit(IMovement movement, Position position)
        {
            _movement = movement;
            _position = position;
            _id = Guid.NewGuid();
        }

        public void MoveTo(Position position)
        {
            _movement = new ToPositionMovement(position, 1);
        }

        public void Update(GameTime gameTime)
        {
            _movement = _movement.Update(ref _position);
        }

        public void RenderTo(Renderer renderer)
        {
            renderer.RenderBlank(_position, new Size(10, 10), Color.Red);
        }

        protected bool Equals(Unit other)
        {
            return _id.Equals(other._id);
        }

        public override bool Equals(object obj)
        {
            if (obj is null)
                return false;

            if (ReferenceEquals(this, obj))
                return true;

            return obj.GetType() == GetType() && Equals((Unit)obj);
        }

        public override int GetHashCode()
        {
            return _id.GetHashCode();
        }
    }
}
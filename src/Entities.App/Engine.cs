﻿using Entities.App.Input;
using Entities.App.Rendering;
using Entities.App.Types;
using Entities.App.Units;
using Microsoft.Xna.Framework;

namespace Entities.App
{
    public class Engine
    {
        private readonly UnitController _unitController;
        private readonly IInputDetector _inputDetector;
        private readonly IRenderables _renderables;

        public Engine(IRenderables renderables, UnitController unitController, IInputDetector inputDetector)
        {
            _renderables = renderables;
            _unitController = unitController;
            _inputDetector = inputDetector;
        }

        public Engine Initialise()
        {
            _unitController.SpawnUnit(new Position(100, 100));
            return this;
        }

        public void Update(GameTime gameTime)
        {
            _inputDetector.Update();
            _unitController.Update(gameTime);
        }

        public void Draw(GameTime gameTime)
        {
            _renderables.RenderAll();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Threading;
using Autofac;

namespace Entities.App.Events
{
    public static class DomainEvents
    {
        private static Lazy<EventBus> _eventAggregator;

        public static void InitialiseFrom(IContainer container)
        {
            _eventAggregator = new Lazy<EventBus>(() => EventBus.From(container.Resolve<IEnumerable<IEventHandler>>()), LazyThreadSafetyMode.ExecutionAndPublication);
        }

        public static void Register<TEvent>(IEventHandler<TEvent> eventHandler) where TEvent : IEvent
        {
            _eventAggregator.Value.Register<TEvent>(eventHandler);
        }

        public static void Publish<TEvent>(TEvent domainEvent) where TEvent : IEvent
        {
            _eventAggregator.Value.Publish(domainEvent);
        }

        public static void Stop()
        {
            _eventAggregator.Value.Dispose();
        }
    }
}
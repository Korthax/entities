﻿using System;
using System.Collections.Generic;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Reflection;

namespace Entities.App.Events
{
    public class EventBus
    {
        private readonly CompositeDisposable _subscriptions;
        private readonly Subject<IEvent> _subject;
        private bool _disposed;

        public static EventBus From(IEnumerable<IEventHandler> handlers)
        {
            var aggregator = new EventBus();
            foreach (var handler in handlers)
            {
                var handerType = handler.GetType();
                foreach (var handlerInterface in handerType.GetInterfaces())
                {
                    MethodInfo addHandlerMethod = null;
                    if (!handlerInterface.IsConstructedGenericType)
                        continue;

                    var requestType = handlerInterface.GenericTypeArguments[0];
                    if (handlerInterface.GetGenericTypeDefinition() == typeof(IEventHandler<>))
                    {
                        var method = typeof(EventBus).GetMethod(nameof(Register), BindingFlags.Instance | BindingFlags.Public);
                        addHandlerMethod = method.MakeGenericMethod(requestType);
                    }

                    addHandlerMethod?.Invoke(aggregator, new object[] { handler });
                }
            }

            return aggregator;
        }

        public EventBus()
        {
            _subject = new Subject<IEvent>();
            _subscriptions = new CompositeDisposable();
        }

        public void Register<TEvent>(object handler) where TEvent : IEvent
        {
            var eventHandler = (IEventHandler<TEvent>)handler;

            var subscription = _subject
                .OfType<TEvent>()
                .AsObservable()
                .Subscribe(eventHandler.Handle);

            _subscriptions.Add(subscription);
        }

        public void Publish<TEvent>(TEvent domainEvent) where TEvent : IEvent
        {
            _subject.OnNext(domainEvent);
        }

        public void Dispose()
        {
            if (_disposed)
                return;

            _subscriptions.Dispose();
            _subject.Dispose();
            _disposed = true;
        }
    }
}
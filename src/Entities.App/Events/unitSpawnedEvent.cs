﻿using Entities.App.Units;

namespace Entities.App.Events
{
    public class UnitSpawnedEvent : IEvent
    {
        public IUnit Unit { get; }

        public UnitSpawnedEvent(IUnit unit)
        {
            Unit = unit;
        }
    }

    public class UnitDestroyedEvent : IEvent
    {
    }
}
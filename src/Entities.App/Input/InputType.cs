﻿namespace Entities.App.Input
{
    public enum InputType
    {
        OnKeyUp,
        OnKeyDown
    }
}
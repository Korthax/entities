﻿using System;

namespace Entities.App.Input
{
    [Flags]
    public enum MouseButtons : uint
    {
        LeftButton = 0,
        RightButton = 1,
        MiddleButton = 2,
        XButton1 = 4,
        XButton2 = 8
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using Entities.App.Events;
using Entities.App.Input.Events;
using Entities.App.Input.Extensions;
using Entities.App.Input.Options;
using Entities.App.Types;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Entities.App.Input
{
    public interface IInputDetector
    {
        void Update();
    }

    public class InputDetector : IInputDetector
    {
        private readonly InputOptions _inputOptions;

        private HashSet<MouseButtons> _oldDownButtons;
        private HashSet<Keys> _oldDownKeys;
        private Position _mousePosition;

        public InputDetector(InputOptions inputOptions)
        {
            _inputOptions = inputOptions;
            _oldDownKeys = new HashSet<Keys>();
            _oldDownButtons = new HashSet<MouseButtons>();

            var mouseState = Mouse.GetState();
            _mousePosition = new Position(mouseState.Position.X, mouseState.Position.Y);
        }

        public void Update()
        {
            var newKeyboardState = Keyboard.GetState();
            var newMouseState = Mouse.GetState();

            var newMousePosition = new Position(newMouseState.Position.X, newMouseState.Position.Y);

            var downKeys = new HashSet<Keys>(newKeyboardState.GetPressedKeys());
            var downButtons = new HashSet<MouseButtons>(newMouseState.GetPressedButtons());

            if(_inputOptions.Type == InputType.OnKeyUp)
            {
                var releasedKeys = new HashSet<Keys>(_oldDownKeys.Where(x => !downKeys.Contains(x)));
                var releasedButtons = new HashSet<MouseButtons>(_oldDownButtons.Where(x => !downButtons.Contains(x)));

                if(releasedKeys.Count > 0 || releasedButtons.Count > 0)
                    DomainEvents.Publish(InputOccurredEvent.From(releasedKeys, releasedButtons, _mousePosition));
            }
            else
            {
                if (downKeys.Count > 0 || downButtons.Count > 0)
                    DomainEvents.Publish(InputOccurredEvent.From(downKeys, downButtons, _mousePosition));
            }

            if(newMousePosition != _mousePosition)
            {
                DomainEvents.Publish(MouseMovedEvent.From(_mousePosition));
                _mousePosition = newMousePosition;
            }

            _oldDownKeys = downKeys;
            _oldDownButtons = downButtons;
        }
    }
}
﻿namespace Entities.App.Input.Options
{
    public class InputOptions
    {
        public InputType Type { get; set; }
    }
}
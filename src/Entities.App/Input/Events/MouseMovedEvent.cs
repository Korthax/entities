using Entities.App.Events;
using Entities.App.Types;

namespace Entities.App.Input.Events
{
    public class MouseMovedEvent : IEvent
    {
        public Position MousePosition { get; private set; }

        public static MouseMovedEvent From(Position mousePosition)
        {
            return new MouseMovedEvent
            {
                MousePosition = mousePosition
            };
        }
    }
}
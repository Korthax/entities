﻿using System.Collections.Generic;
using Entities.App.Events;
using Entities.App.Types;
using Microsoft.Xna.Framework.Input;

namespace Entities.App.Input.Events
{
    public class InputOccurredEvent : IEvent
    {
        public Position MousePosition { get; private set; }
        public ModifierKeys Modifiers { get; private set; }
        public HashSet<Keys> KeySet { get; private set; }
        public HashSet<MouseButtons> ButtonSet { get; private set; }

        public static InputOccurredEvent From(HashSet<Keys> keys, HashSet<MouseButtons> buttons, Position mousePosition)
        {
            var modifiers = ModifierKeys.None;

            if (keys.Contains(Keys.LeftAlt))
                modifiers |= ModifierKeys.LeftAlt;

            if (keys.Contains(Keys.RightAlt))
                modifiers |= ModifierKeys.RightAlt;

            if (keys.Contains(Keys.LeftShift))
                modifiers |= ModifierKeys.LeftShift;

            if (keys.Contains(Keys.RightShift))
                modifiers |= ModifierKeys.RightShift;

            if (keys.Contains(Keys.LeftControl))
                modifiers |= ModifierKeys.LeftControl;

            if (keys.Contains(Keys.RightControl))
                modifiers |= ModifierKeys.RightControl;

            return new InputOccurredEvent
            {
                MousePosition = mousePosition,
                Modifiers = modifiers,
                ButtonSet = buttons,
                KeySet = keys
            };
        }
    }
}
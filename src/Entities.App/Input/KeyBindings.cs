﻿using System.Collections.Generic;
using Entities.App.Units;

namespace Entities.App.Input
{
    public class KeyBindings
    {
        public Dictionary<UnitAction, MouseButtons> Actions { get; }

        public static KeyBindings From(Dictionary<UnitAction, MouseButtons> actions)
        {
            var unitActions = new Dictionary<UnitAction, MouseButtons>
            {
                [UnitAction.Move] = MouseButtons.RightButton
            };

            return new KeyBindings(unitActions);
        }

        public KeyBindings(Dictionary<UnitAction, MouseButtons> unitActions)
        {
            Actions = unitActions;
        }
    }
}
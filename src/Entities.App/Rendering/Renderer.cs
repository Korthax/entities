﻿using System.Collections.Generic;
using Entities.App.Assets;
using Entities.App.Events;
using Entities.App.Types;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Entities.App.Rendering
{
    public class Renderer
    {
        private readonly AssetLoader _assetLoader;
        private readonly SpriteBatch _spriteBatch;

        public Renderer(SpriteBatch spriteBatch, AssetLoader assetLoader)
        {
            _spriteBatch = spriteBatch;
            _assetLoader = assetLoader;
        }

        public void Begin()
        {
            _spriteBatch.Begin();
        }

        public void RenderBlank(Position position, Size size, Color color)
        {
            _spriteBatch.Draw(_assetLoader.Blank(), new Rectangle((int)position.X, (int)position.Y, (int)size.Width, (int)size.Height), color);
        }

        public void End()
        {
            _spriteBatch.End();
        }
    }

    public interface IRenderables
    {
        void RenderAll();
    }

    public class Renderables : IEventHandler<UnitSpawnedEvent>, IEventHandler<UnitDestroyedEvent>, IRenderables
    {
        private readonly Dictionary<int, IRenderable> _items;
        private readonly Renderer _renderer;

        public Renderables(Renderer renderer)
        {
            _renderer = renderer;
            _items = new Dictionary<int, IRenderable>();
        }

        public void RenderAll()
        {
            _renderer.Begin();

            foreach (var renderable in _items.Values)
                renderable.RenderTo(_renderer);

            _renderer.End();
        }

        public void Handle(UnitSpawnedEvent domainEvent)
        {
            if (domainEvent.Unit is IRenderable renderable)
                _items.Add(renderable.GetHashCode(), renderable);
        }

        public void Handle(UnitDestroyedEvent domainEvent)
        {
            var key = domainEvent.GetHashCode();

            if (_items.ContainsKey(key))
                _items.Remove(key);
        }
    }

    public interface IRenderable
    {
        void RenderTo(Renderer renderer);
    }
}